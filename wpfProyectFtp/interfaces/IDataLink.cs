﻿using BibliotecaClasess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.interfaces
{
    public interface IDatosLink
    {
        public OperationResult<Datoslink> GetListaHipervinculos();
        public OperationResult<Datoslink> InsertHipervinculos(Datoslink datoslink);
        public OperationResult<Datoslink> UpdateHipervinculos();
        public OperationResult<Datoslink> DeleteHipervinculos();




    }
}
