﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.models
{
    public class DbContextMysql
    {
        public string ConnectionString { get; set; }
        public DbContextMysql(string conectionString)
        {
            this.ConnectionString = conectionString;
        }

        private MySqlConnection GetConnection() {
            return new MySqlConnection(ConnectionString);
        }
    }
}
