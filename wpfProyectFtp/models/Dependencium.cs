﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Dependencium
    {
        public short Id { get; set; }
        public string NombreDepartamento { get; set; }
    }
}
