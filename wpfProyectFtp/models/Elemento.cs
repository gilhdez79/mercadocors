﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Elemento
    {
        public short Id { get; set; }
        public string NombreElemento { get; set; }
    }
}
