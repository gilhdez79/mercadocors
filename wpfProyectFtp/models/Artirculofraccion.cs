﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Artirculofraccion
    {
        public short Id { get; set; }
        public short? IdArticulo { get; set; }
        public short? IdFraccion { get; set; }
        public short? IdClase { get; set; }
        public short? IdTrimestre { get; set; }

        public virtual Articulo IdArticuloNavigation { get; set; }
        public virtual Elementosclase IdClaseNavigation { get; set; }
        public virtual Fraccion IdFraccionNavigation { get; set; }
        public virtual Trimestre IdTrimestreNavigation { get; set; }
    }
}
