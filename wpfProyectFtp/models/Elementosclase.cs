﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Elementosclase
    {
        public Elementosclase()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string Clase { get; set; }

        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
