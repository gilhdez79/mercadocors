﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Trimestre
    {
        public Trimestre()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string Numero { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
