﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Archivo
    {
        public int Id { get; set; }
        public string NombreArchivo { get; set; }
        public string PesoMb { get; set; }
        public string RutaArchivo { get; set; }
    }
}
