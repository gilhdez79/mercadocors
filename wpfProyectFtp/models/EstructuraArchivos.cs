﻿using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.interfaces;

namespace wpfProyectFtp.models
{
  public   class EstructuraArchivo
    {
        public Guid idArchivo { get; set; }
        public string Annio { get; set; }
        public string Aticulo { get; set; }

        public string Fraccion { get; set; }
        public string LinkGenerado { get; set; }
        public string LinkHtml { get; set; }
        public string  Trimestre { get; set; }


    }
}
