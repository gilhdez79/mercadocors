﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class dbarchivostransparenciaContext : DbContext
    {
        public dbarchivostransparenciaContext()
        {
        }

        public dbarchivostransparenciaContext(DbContextOptions<dbarchivostransparenciaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Anio> Anios { get; set; }
        public virtual DbSet<Archivo> Archivos { get; set; }
        public virtual DbSet<Articulo> Articulos { get; set; }
        public virtual DbSet<Artirculofraccion> Artirculofraccions { get; set; }
        public virtual DbSet<Datoslink> Datoslinks { get; set; }
        public virtual DbSet<Dependencium> Dependencia { get; set; }
        public virtual DbSet<Elemento> Elementos { get; set; }
        public virtual DbSet<Elementosclase> Elementosclases { get; set; }
        public virtual DbSet<Fraccion> Fraccions { get; set; }
        public virtual DbSet<Trimestre> Trimestres { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Usuariodependencium> Usuariodependencia { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=123456;database=dbarchivostransparencia");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Anio>(entity =>
            {
                entity.ToTable("anios");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Anio1)
                    .HasMaxLength(25)
                    .HasColumnName("Anio")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Archivo>(entity =>
            {
                entity.ToTable("archivo");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.NombreArchivo)
                    .HasMaxLength(150)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PesoMb)
                    .HasMaxLength(15)
                    .HasColumnName("PesoMB")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.RutaArchivo)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Articulo>(entity =>
            {
                entity.ToTable("articulos");

                entity.Property(e => e.Id).HasColumnType("smallint(6)");

                entity.Property(e => e.EstructuraCarpeta)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.NombreArticulo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Titulo)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Artirculofraccion>(entity =>
            {
                entity.ToTable("artirculofraccion");

                entity.HasIndex(e => e.IdArticulo, "ArtirculoFraccion_FK");

                entity.HasIndex(e => e.IdFraccion, "ArtirculoFraccion_FK_1");

                entity.HasIndex(e => e.IdClase, "artirculofraccion_FK_clase");

                entity.HasIndex(e => e.IdTrimestre, "artirculofraccion_FK_trimestre");

                entity.Property(e => e.Id)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("id");

                entity.Property(e => e.IdArticulo)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("idArticulo")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdClase)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("idClase")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdFraccion)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("idFraccion")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdTrimestre)
                    .HasColumnType("smallint(6)")
                    .HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.IdArticuloNavigation)
                    .WithMany(p => p.Artirculofraccions)
                    .HasForeignKey(d => d.IdArticulo)
                    .HasConstraintName("ArtirculoFraccion_FK");

                entity.HasOne(d => d.IdClaseNavigation)
                    .WithMany(p => p.Artirculofraccions)
                    .HasForeignKey(d => d.IdClase)
                    .HasConstraintName("artirculofraccion_FK_clase");

                entity.HasOne(d => d.IdFraccionNavigation)
                    .WithMany(p => p.Artirculofraccions)
                    .HasForeignKey(d => d.IdFraccion)
                    .HasConstraintName("ArtirculoFraccion_FK_1");

                entity.HasOne(d => d.IdTrimestreNavigation)
                    .WithMany(p => p.Artirculofraccions)
                    .HasForeignKey(d => d.IdTrimestre)
                    .HasConstraintName("artirculofraccion_FK_trimestre");
            });

            modelBuilder.Entity<Datoslink>(entity =>
            {
                entity.ToTable("datoslinks");

                entity.HasIndex(e => e.IdArticuloFraccion, "datoslinks_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(200)")
                    .HasColumnName("ID");

                entity.Property(e => e.FechaCreacion)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdArticuloFraccion)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("idArticuloFraccion")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Link)
                    .HasMaxLength(600)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LinklHtml).HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NombreArchivo)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PesoKb)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UrlCarpeta)
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Dependencium>(entity =>
            {
                entity.ToTable("dependencia");

                entity.Property(e => e.Id)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("id");

                entity.Property(e => e.NombreDepartamento)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Elemento>(entity =>
            {
                entity.ToTable("elementos");

                entity.Property(e => e.Id).HasColumnType("smallint(6)");

                entity.Property(e => e.NombreElemento)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Elementosclase>(entity =>
            {
                entity.ToTable("elementosclase");

                entity.Property(e => e.Id).HasColumnType("smallint(6)");

                entity.Property(e => e.Clase)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Fraccion>(entity =>
            {
                entity.ToTable("fraccion");

                entity.Property(e => e.Id).HasColumnType("smallint(6)");

                entity.Property(e => e.EstructuraCarpeta)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Trimestre>(entity =>
            {
                entity.ToTable("trimestre");

                entity.Property(e => e.Id)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("id");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Numero)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("usuario");

                entity.Property(e => e.Id)
                    .HasColumnType("smallint(6)")
                    .HasColumnName("id");

                entity.Property(e => e.Amaterno)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ApellidoP)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CorreoElectronico)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Pasword)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Usuario1)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("Usuario");
            });

            modelBuilder.Entity<Usuariodependencium>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("usuariodependencia");

                entity.HasIndex(e => e.IdUsuario, "UsuarioDependencia_FK");

                entity.HasIndex(e => e.IdDependencia, "UsuarioDependencia_FK_1");

                entity.Property(e => e.IdDependencia)
                    .HasColumnType("smallint(6)")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdUsuario)
                    .HasColumnType("smallint(6)")
                    .HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.IdDependenciaNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdDependencia)
                    .HasConstraintName("UsuarioDependencia_FK_1");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("UsuarioDependencia_FK");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
