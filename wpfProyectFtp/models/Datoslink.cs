﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Datoslink
    {
        public long Id { get; set; }
        public string UrlCarpeta { get; set; }
        public string Link { get; set; }
        public string LinklHtml { get; set; }
        public short? IdArticuloFraccion { get; set; }
        public string FechaCreacion { get; set; }
        public string PesoKb { get; set; }
        public string NombreArchivo { get; set; }
    }
}
