﻿using System;
using System.Collections.Generic;

#nullable disable

namespace wpfProyectFtp.Models
{
    public partial class Fraccion
    {
        public Fraccion()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string Nombre { get; set; }
        public string EstructuraCarpeta { get; set; }

        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
