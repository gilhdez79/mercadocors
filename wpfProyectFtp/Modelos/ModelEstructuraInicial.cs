﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.Modelos
{
    public class ModelEstructuraInicial
    {
        public int anio { get; set; }
        public ModelEstructuraInicial(string anio)
        {
            _carpetaPricipal = $"Transparencia_{anio}";
            _subCarpetas = new List<string> { "Articulos",
                                            "Informes_Anuales",
                                            "Cuenta_Publica",
                                            "Leyes",
                                            "Disciplina_Financiera"};
            _trimestres = new List<string> {"1er_trimestre", "2do_trimestre", "3er_trimestre", "4to_trimestre" };
            _trimestres = new List<string> { "Art_76", "Art_78"};
        }

        private string _carpetaPricipal;

        public string CarpetaPricipals
        {
            get { return _carpetaPricipal; }
            set { _carpetaPricipal = value; }
        }
        private List<string> _subCarpetas;

        public List<string> SubCarpetas
        {
            get { return _subCarpetas; }
            set { _subCarpetas = value; }
        }

        private List<string> _fracciones;

        public List<string> Fracciones
        {
            get { return _subCarpetas; }
            set { _subCarpetas = value; }
        }

        private List<string> _trimestres;

        public List<string> Trimestres
        {
            get { return _subCarpetas; }
            set { _subCarpetas = value; }
        }
        private List<string> _articulos;
        public List<string> Articulos
        {
            get { return _articulos; }
            set { _articulos = value; }
        }

    }
}
