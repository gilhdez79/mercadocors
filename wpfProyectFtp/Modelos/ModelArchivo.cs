﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.Modelos
{
   public class ModelArchivo
    {
        private int idArchivo;

        public int IdArchivo
        {
            get { return idArchivo; }
            set { idArchivo = value; }
        }

        private string nombreArchivo;

        public string NombreArchivo
        {
            get { return nombreArchivo; }
            set { nombreArchivo = value; }
        }
        private string  rutaArchivo;

        public string RutaArchivo
        {
            get { return rutaArchivo; }
            set { rutaArchivo = value; }
        }

        private string pesoMb;

        public string PesoMB
        {
            get { return pesoMb; }
            set { pesoMb = value; }
        }

        private string fechaCreacion;

        public string FechaCreacion
        {
            get { return fechaCreacion; }
            set { fechaCreacion = value; }
        }


    }
}
