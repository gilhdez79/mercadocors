﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace wpfProyectFtp.Modelos
{
    public class ObsrvButtons : INotifyPropertyChanged
    {
        private static readonly ObsrvButtons _instance = new ObsrvButtons();
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string pName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(pName));
            }
        }

        bool _btnBuscar;

        public bool BtnBuscarEnable
        {
            get { return _btnBuscar; }
            set
            {
                _btnBuscar = value;
                OnPropertyChanged("BtnBuscarEnable");
            }
        }

        bool _btnGeneraLink;

        public bool BtnGeneraLinkEnable
        {
            get { return _btnGeneraLink; }
            set
            {
                _btnGeneraLink = value;
                OnPropertyChanged("BtnGeneraLinkEnable");
            }
        }
        bool _btnSubirPortal;

        public bool BtnSubirPortalEnable
        {
            get { return _btnSubirPortal; }
            set
            {
                _btnSubirPortal = value;
                OnPropertyChanged("BtnSubirPortalEnable");
            }
        }
        bool _btncopiar;

        public bool BtnCopiarEnable
        {
            get { return _btncopiar; }
            set
            {
                _btncopiar = value;
                OnPropertyChanged("BtnCopiarEnable");
            }
        }

        public void DisabledButtons()
        {
            this.BtnBuscarEnable = false;
            this.BtnCopiarEnable = false;
            this.BtnGeneraLinkEnable= false;
            this.BtnSubirPortalEnable = false;
        }
        public static ObsrvButtons GetButtonsInfo()
        {
            return _instance;
        }

    }
}
