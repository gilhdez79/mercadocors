﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.Modelos
{
  public  class ModelEstructuraRutaRequest
    {
        private int  anio;

        public int Annio
        {
            get { return anio; }
            set { anio = value; }
        }

        private int clasificacionArchivo;

        public int ClasificacionArchivo
        {
            get { return clasificacionArchivo; }
            set { clasificacionArchivo = value; }
        }

        private int articulo;

        public int Articulo
        {
            get { return articulo; }
            set { articulo = value; }
        }

        private int fracccion;

        public int Fraccion
        {
            get { return fracccion; }
            set { fracccion = value; }
        }
        private int inciso;

        public int Inciso
        {
            get { return inciso; }
            set { inciso = value; }
        }
        private int trimestre;

        public int Trimestre
        {
            get { return trimestre; }
            set { trimestre = value; }
        }

       
        private int urlFtp;
       
        public int UrlFtp
        {
            get { return urlFtp; }
            set { urlFtp = value; }
        }

        private string hypervinculo;

        public string HyperVinculo
        {
            get { return hypervinculo; }
            set { hypervinculo = value; }
        }


    }
}
