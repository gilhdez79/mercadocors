﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpfProyectFtp.Modelos
{
  public  class ModelEstructuraRuta
    {
        private string  anio;
        public ModelEstructuraRuta()
        {
            _modeloArchivo = new ModelArchivo
            {
                IdArchivo = 0,
                FechaCreacion = DateTime.Now.ToString(),
                NombreArchivo = string.Empty,
                PesoMB = string.Empty,
                RutaArchivo = string.Empty
            };
        }

        public string  Annio
        {
            get { return anio; }
            set { anio = value; }
        }

        private string  clasificacionArchivo;

        public string  ClasificacionArchivo
        {
            get { return clasificacionArchivo; }
            set { clasificacionArchivo = value; }
        }

        private string articulo;

        public string Articulo
        {
            get { return articulo; }
            set { articulo = value; }
        }

        private string fracccion;

        public string Fraccion
        {
            get { return fracccion; }
            set { fracccion = value; }
        }
        private string inciso;

        public string Inciso
        {
            get { return inciso; }
            set { inciso = value; }
        }
        private string trimestre;

        public string Trimestre
        {
            get { return trimestre; }
            set { trimestre = value; }
        }
        private string urlFtp;
        public string UrlFtp
        {
            get { return urlFtp; }
            set { urlFtp = value; }
        }

        private string estructuraEnCarpetaFTP;

        public string EstructuraEnCarpetaFTP
        {
            get { return estructuraEnCarpetaFTP; }
            set { estructuraEnCarpetaFTP = value; }
        }


        private string hypervinculo;

        public string HyperVinculo
        {
            get { return hypervinculo; }
            set { hypervinculo = value; }
        }

        private ModelArchivo _modeloArchivo;

        public ModelArchivo _ModelArchivo
        {
            get { return _modeloArchivo; }
            set { _modeloArchivo = value; }
        }


        public string GeneraHyperVinculo()
        {
            string urlFtp = string.Empty;
            
            string strRaiz = $"{Resources.NombreCarpeta_Text}{anio}"; 

            string urlEstructura = string.Empty;
            string urlFraccion = $"fracc_{fracccion}{inciso}";
            string urlArticulo = $"Art{articulo}";
            switch (this.clasificacionArchivo)
            {
                case "Articulo": {
                        urlEstructura = $"{strRaiz}/{urlArticulo}/{urlFraccion}/{trimestre}";
                        break; 
                    }
                default: {
                        urlEstructura = $"{strRaiz}/{clasificacionArchivo}/{trimestre}";
                        break;
                    }
            }

            return urlEstructura;
        }
    }
}
