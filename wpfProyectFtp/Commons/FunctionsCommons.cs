﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wpfProyectFtp.Commons
{
  public  class FunctionsCommons
    {
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public List<string> GetListaAnnios()
        {
            List<string> listaAnnios = new List<string>();
            int annioActual = DateTime.Now.Year;
            int annioinicial = 2018;

            for (int i = annioinicial; i <= annioActual; i++)
            {
                listaAnnios.Add(i.ToString());
            }

            var listaOrdenada = from item in listaAnnios

                    orderby item descending
                    select item;

           // listaAnnios = listaAnnios.OrderBy(p => p.Substring(0)).ToList(); ;

            return listaOrdenada.ToList();
        }

        public Dictionary<string, string> GetListTrimestres()
        {
          
            Dictionary<string, string> listTrimestre = new Dictionary<string, string>();
            listTrimestre.Add("1er_trimestre", "Trimestre I");
            listTrimestre.Add("2do_trimestre", "Trimestre II");
            listTrimestre.Add("3er_trimestre", "Trimestre III");
            listTrimestre.Add("4to_trimestre", "Trimestre IV");
           
            
            return listTrimestre;

             
        }


    }
}
