﻿using BibliotecaClasess.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using wpfProyectFtp.businessLogic;

namespace wpfProyectFtp.Commons
{

        public class ClsViewModelArticulos
    {
        static ClsViewModelArticulos()
        {
            
        }
        private static string _selectedItem;
        public static string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
              
            }
        }

        private static List<Articulo> _items;
        public static List<Articulo> Items
        {
            get { return _items; }
            set { _items = value; }
        }
    }
}

