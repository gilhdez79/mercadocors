﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using BibliotecaClasess.Models;
using Newtonsoft.Json;
using wpfProyectFtp.Modelos;

namespace wpfProyectFtp.Commons
{
   public class APIConnect<T>
    {
        string BaseUri = $"http://localhost:64189/api/";
        public dynamic EjecGetApi(T requestObj)
        {
            dynamic objDinamico = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:64189/api/");
                //HTTP GET
                var responseTask = client.GetAsync("student");
                responseTask.Wait();

                // Setting content type.  
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Setting timeout.  
                client.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(1000000));

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                
                var parameters = JsonConvert.SerializeObject(requestObj);

                // HTTP POST  



              //  response = client.PostAsync("api/WebApi/PostRegInfo", new FormUrlEncodedContent(parameters));

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                  

                    objDinamico = result;
                }
                else //web api sent error response
                {
                    //log response status here..
                   
                }
            }

            return objDinamico;
        }

      public async Task<string> GetAsync(T data, string _resource)
        {
            string dataObjects = string.Empty;
            BibliotecaClasess.Models.OperationResult<T> operationresult = new BibliotecaClasess.Models.OperationResult<T>();

            string uriRequest = Resources.UrlApi_Text.ToString();
            //var parameters = new Dictionary<string, string>();
            //parameters["text"] = text;

            //var response = await httpClient.PostAsync(BaseUri, new FormUrlEncodedContent(parameters));
            //var contents = await response.Content.ReadAsStringAsync();

            //return contents;

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(uriRequest);
                string urlCompleta = _resource;
                

                //var myContent = JsonConvert.SerializeObject(data);
                //var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                //var byteContent = new ByteArrayContent(buffer);
                //byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(
           new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(urlCompleta).Result;
              

                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    //var dataObjects = response.Content.r<IEnumerable<DataObject>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                    //foreach (var d in dataObjects)
                    //{
                    //    Console.WriteLine("{0}", d.Name);
                    //}

                    using (HttpContent content = response.Content)
                    {
                          dataObjects = await content.ReadAsStringAsync().ConfigureAwait(false);
                      //  var deserializado = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<T>>( dataObjects);
                       // operationresult.InfoMensaje = deserializado("InfoMensaje");
                       
                      //  return dataObjects;
                    }
                }
            }
            return dataObjects;
        }

      public async Task<string> PostAsync(T data, string _resource)
        {
            string dataObjects = string.Empty;
            BibliotecaClasess.Models.OperationResult<T> operationresult = new BibliotecaClasess.Models.OperationResult<T>();

            string uriRequest = Resources.UrlApi_Text.ToString();
            //var parameters = new Dictionary<string, string>();
            //parameters["text"] = text;

            //var response = await httpClient.PostAsync(BaseUri, new FormUrlEncodedContent(parameters));
            //var contents = await response.Content.ReadAsStringAsync();

            //return contents;

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(uriRequest);
                string urlCompleta = _resource;


                //var myContent = JsonConvert.SerializeObject(data);
                //var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                //var byteContent = new ByteArrayContent(buffer);
                //byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                /* var content = new StringContent(
                         JsonConvert.SerializeObject(data, "application/json")
                         ) ;

                 client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

                 var response =  await client.PostAsync(urlCompleta, content).Result;*/
                HttpContent contentPost = new StringContent(
                        JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                var response =  await client.PostAsync(urlCompleta, contentPost).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    //var dataObjects = response.Content.r<IEnumerable<DataObject>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                    //foreach (var d in dataObjects)
                    //{
                    //    Console.WriteLine("{0}", d.Name);
                    //}

                    using (HttpContent content = response.Content)
                    {
                        dataObjects = await content.ReadAsStringAsync().ConfigureAwait(false);
                        //  var deserializado = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<T>>( dataObjects);
                        // operationresult.InfoMensaje = deserializado("InfoMensaje");

                        //  return dataObjects;
                    }
                }
            }
            return dataObjects;
        }

    }
}
