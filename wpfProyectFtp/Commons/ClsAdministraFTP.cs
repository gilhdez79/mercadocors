﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using wpfProyectFtp.Modelos;
using System.Linq;

namespace wpfProyectFtp.Commons
{
  public  class ClsAdministraFTP
    {
        /// <summary>
        /// Metodo para subir archivo al servidor FTP de Transparencia
        /// </summary>
        /// <param name="urlArchivo">Ruta completa del Archivo</param>
        /// <param name="estructuraCarpetas">Estructura de la localizacion del Archivo (Sus Carpetas)</param>
        /// <returns></returns>
        public bool SubirArchivoaFTP( ModelEstructuraRuta modelEstructura)

        {
            bool resultado = false;
            string httpdocs = @"/httpdocs/";
            //
            try
            {

                bool esCorrctalaUrl = MakeFTPDir(modelEstructura.EstructuraEnCarpetaFTP);


                if (esCorrctalaUrl)
                {
                    string rutaArhivo = modelEstructura._ModelArchivo.RutaArchivo;
                    //string rutaArhivo = @"C:\AyuntamientoCardenas\Documentos Transparencia\Articulo 78\fracc_XIV\Actas Secretaria_2021\2do_trimestre\2DO TRIM 2021 Acta 77 SESION 30 ABRIL.pdf";
                    string ftpServerIP = Recursos.FtpServerIp_Text;
                    FileInfo fileInf = new FileInfo(rutaArhivo);

                    //string uri = "ftp://" + ftpServerIP + "/" + fileInf.Name;

                    FtpWebRequest reqFTP;

                    // Create FtpWebRequest object from the Uri provided

                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + httpdocs + modelEstructura.EstructuraEnCarpetaFTP + "/" + fileInf.Name));

                    //            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + "/httpdocs/Transparencia_2019/art_78/fracc_VII/4to_trimestre/" + fileInf.Name));

                    // Provide the WebPermission Credintials

                    reqFTP.Credentials = new NetworkCredential("transpa1", "yt%vrX7!%7$");

                    // By default KeepAlive is true, where the control connection is not closed

                    // after a command is executed.

                    reqFTP.KeepAlive = false;

                    // Specify the command to be executed.

                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

                    // Specify the data transfer type.

                    reqFTP.UseBinary = true;

                    // Notify the server about the size of the uploaded file

                    reqFTP.ContentLength = fileInf.Length;

                    // The buffer size is set to 2kb

                    int buffLength = 2048;

                    byte[] buff = new byte[buffLength];

                    int contentLen;

                    // Opens a file stream (System.IO.FileStream) to read the file to be uploaded

                    FileStream fs = fileInf.OpenRead();



                    // Stream to which the file to be upload is written

                    Stream strm = reqFTP.GetRequestStream();

                    // Read from the file stream 2kb at a time

                    contentLen = fs.Read(buff, 0, buffLength);

                    // Until Stream content ends

                    while (contentLen != 0)

                    {

                        // Write Content from the file stream to the FTP Upload Stream

                        strm.Write(buff, 0, contentLen);

                        contentLen = fs.Read(buff, 0, buffLength);

                        resultado = true;
                    }

                    // Close the file stream and the Request Stream

                    strm.Close();

                    fs.Close(); 
                }

            }

            catch (Exception ex)

            {

                Console.WriteLine(ex.Message);
            }

            return resultado;

        }

        /// <summary>
        /// Verifica si existe el folder en la ruta
        /// </summary>
        /// <param name="carpetaBuscar">Carpeta donde se alojará el Archivo</param>
        /// <param name="folderRaiz">Ruta Madre donde existirá la carpeta</param>
        /// <returns></returns>
        public bool ExistFoldersintoDirectory(string carpetaBuscar, string buscarEnRuta )
        {
            // Art78
            bool result = false;
           // folderRaiz = "Transparencia_2021/Art76";
            //Transparencia_2021/Art78
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"ftp://transparencia.cardenas.gob.mx/httpdocs/{buscarEnRuta}");
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential("transpa1", "yt%vrX7!%7$");

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            List<string> directories = new List<string>();

            string line = reader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                string[] arr = line.Split(' ');
                List<string>list = new List<string>(arr);
               
                directories.Add(list.Last());
                line = reader.ReadLine();

                
            }

            reader.Close();
            response.Close();

            if (directories.Count>0)
            {
                if (directories.Contains(carpetaBuscar))
                {
                    result =  true;
                }
                else { result =  false; }

            }

            return result;
            
        }

        public bool  MakeFTPDir(string pathToCreate)
        {
            bool result = false;
            ClsAdministraFTP clsAdministraFTP = new ClsAdministraFTP();
            FtpWebRequest reqFTP = null;
            Stream ftpStream = null;

            string[] subDirs = pathToCreate.Split('/');

            string currentDir = string.Format("ftp://{0}/{1}", Recursos.FtpServerIp_Text, "httpdocs");
            string strPrincipal = subDirs[0];
            string strComplementaria = string.Empty;
            string directorio = string.Empty;
            foreach (string subDir in subDirs)
            {
               
                try
                {
                    currentDir = currentDir + "/" + subDir;
                    
                    if (string.Equals(strPrincipal, subDir))
                    {
                        strComplementaria += subDir;

                        bool existelacarpeta = clsAdministraFTP.ExistFoldersintoDirectory(strPrincipal, string.Empty);

                    }
                    else
                    {
                       
                        bool existelacarpeta = clsAdministraFTP.ExistFoldersintoDirectory(subDir, directorio);

                        if (!existelacarpeta)
                        {
                            reqFTP = (FtpWebRequest)FtpWebRequest.Create(currentDir);
                            reqFTP.Method = WebRequestMethods.Ftp.MakeDirectory;
                            reqFTP.UseBinary = true;
                            reqFTP.Credentials = new NetworkCredential("transpa1", "yt%vrX7!%7$");
                            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                            ftpStream = response.GetResponseStream();
                            ftpStream.Close();
                            response.Close();

                            strComplementaria += "/"+ subDir;
                        }
                        
                    }


                    directorio += "/" + subDir;
                }
                catch (Exception ex)
                {
                    return false;
                    //directory already exist I know that is weak but there is no way to check if a folder exist on ftp...
                }

                result = true;
            }

            return result;
        }

        public bool VerificaYCreaEstructuraCarpeta(int anio)
        {
          var ruta = "Transparencia_2019/art_78/fracc_VII/4to_trimestre";
            var arrRuta = ruta.Split("/");

            ModelEstructuraInicial modelEstructura = new ModelEstructuraInicial(anio.ToString());

            foreach (var item in arrRuta)
            {
                 
            }
            return true;
        }

    }
}
