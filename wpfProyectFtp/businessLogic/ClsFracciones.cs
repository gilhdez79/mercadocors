﻿using BibliotecaClasess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.Commons;

namespace wpfProyectFtp.businessLogic
{
   public class ClsFracciones
    {
        public OperationResult<List<Fraccion>> GetListaFracciones()
        {
            List<Articulo> listaArticulos = new List<Articulo>();
            OperationResult<List<Fraccion>> _objArticulo = new OperationResult<List<Fraccion>>();
            try
            {
                Fraccion objUsuario = new Fraccion();
                APIConnect<Fraccion> apiUsuarios = new APIConnect<Fraccion>();
                string recurso = $"fraccion/GetFracciones";

                var result = apiUsuarios.GetAsync(objUsuario, recurso).Result;
                _objArticulo = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<List<Fraccion>>>(result);


            }
            catch (Exception ex)
            {
                _objArticulo.InfoMensaje.Message = ex.Message.ToString();
                _objArticulo.InfoMensaje.TipoMensaje = TipoMensaje.Error;
                throw ex;
            }

            return _objArticulo;
        }
    }
}
