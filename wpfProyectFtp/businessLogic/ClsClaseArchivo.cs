﻿using BibliotecaClasess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.Commons;

namespace wpfProyectFtp.businessLogic
{
  public  class ClsClaseArchivo
    {
        public OperationResult<List<Elemento>> GetListaClaseArchivo()
        {
            List<Elemento> listaElementos = new List<Elemento>();
            OperationResult<List<Elemento>> _objElemento = new OperationResult<List<Elemento>>();
            try
            {
                Elemento objElemento = new Elemento();
                APIConnect<Elemento> apiUsuarios = new APIConnect<Elemento>();
                string recurso = $"clasearchivo/GetClaseArchivos";

                var result = apiUsuarios.GetAsync(objElemento, recurso).Result;
                _objElemento = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<List<Elemento>>>(result);

            }
            catch (Exception ex)
            {
                _objElemento.InfoMensaje.Message = ex.Message.ToString();
                _objElemento.InfoMensaje.TipoMensaje = TipoMensaje.Error;
                throw ex;
            }

            return _objElemento;
        }
    }
}
