﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace wpfProyectFtp.businessLogic
{
   public class ClsGeneraLinks
    {
        public static List<string> GetNameFiles(string trimestre)
        {
            string rutaFtp = $@"http://transparencia.cardenas.gob.mx/Transparencia_2018/art_76/fracc_XXIX/{trimestre}/";

            List<string> lista = new List<string>();

            string root = $@"C:\AyuntamientoCardenas\Documentos Transparencia\Articulo 76\Informes DIF en PDF, Año 2018\{trimestre}";
            string[] fileEntries = Directory.GetFiles(root);
            Console.WriteLine(fileEntries.Length.ToString());

            foreach (string fileName in fileEntries)
            {
                FileInfo fi = new FileInfo(fileName);
                var rutaHtml = $"{fi.Name} \t {rutaFtp + fi.Name.Replace(" ", "%20")}";
                lista.Add(rutaHtml);
            }

            return lista;
        }

        public static void TextWriter(List<string> lista, string trimestre)
        {
            string folder = $@"C:\AyuntamientoCardenas\Documentos Transparencia\Articulo 76\Informes DIF en PDF, Año 2018\{trimestre}\";

            string fileName = $"ArchivosLink_{trimestre}_{DateTime.Now.ToFileTime()}.txt";

            string fullPath = folder + fileName;

            using (TextWriter tw = new StreamWriter(fileName))
            {
                tw.WriteLine("NombreArchivo \t Ruta");

                foreach (String s in lista)
                    tw.WriteLine(s);
            }
        }
    }
}
