﻿using BibliotecaClasess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.Commons;
 

namespace wpfProyectFtp.businessLogic
{
  public  class ClsInciso
    {
        public BibliotecaClasess.Models.OperationResult<List<Elementosclase>> GetListaElementosClases()
        {
            List<Elementosclase> listaElementosClases = new List<Elementosclase>();
            BibliotecaClasess.Models.OperationResult<List<Elementosclase>> _objElementosClase = new OperationResult<List<Elementosclase>>();
            try
            {
                Elementosclase objUsuario = new Elementosclase();
                APIConnect<Elementosclase> apiUsuarios = new APIConnect<Elementosclase>();
                string recurso = $"inciso/GetInciso";

                var result = apiUsuarios.GetAsync(objUsuario, recurso).Result;
                _objElementosClase = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<List<Elementosclase>>>(result);


            }
            catch (Exception ex)
            {
                _objElementosClase.InfoMensaje.Message = ex.Message.ToString();
                _objElementosClase.InfoMensaje.TipoMensaje = TipoMensaje.Error;
                throw ex;
            }

            return _objElementosClase;
        }
    }
}
