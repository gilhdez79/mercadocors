﻿using BibliotecaClasess.Models;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.interfaces;
using wpfProyectFtp.Models;
using Newtonsoft.Json;
using wpfProyectFtp.Commons;

namespace wpfProyectFtp.businessLogic
{
    public class ClsDatosLink : IDatosLink
    {
        public OperationResult<BibliotecaClasess.Models.Datoslink> DeleteHipervinculos()
        {
            throw new NotImplementedException();
        }

        public OperationResult<BibliotecaClasess.Models.Datoslink> GetListaHipervinculos()
        {
            throw new NotImplementedException();
        }

        public OperationResult<BibliotecaClasess.Models.Datoslink> InsertHipervinculos(BibliotecaClasess.Models.Datoslink _datosLink)
        {
          //  BibliotecaClasess.Models.Datoslink _datosLink = new BibliotecaClasess.Models.Datoslink();
            OperationResult<BibliotecaClasess.Models.Datoslink> operationResult = new OperationResult<BibliotecaClasess.Models.Datoslink>();
            try
            {
                
                APIConnect<BibliotecaClasess.Models.Datoslink> apiUsuarios = new APIConnect<BibliotecaClasess.Models.Datoslink>();
                string recurso = $"datoslink/";

                var result = apiUsuarios.PostAsync(_datosLink, recurso).Result;
                operationResult = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<BibliotecaClasess.Models.Datoslink>>(result);


            }
            catch (Exception ex)
            {
                operationResult.InfoMensaje.Message = ex.Message.ToString();
                operationResult.InfoMensaje.TipoMensaje = TipoMensaje.Error;
                throw ex;
            }

            return operationResult;
        }

        public OperationResult<BibliotecaClasess.Models.Datoslink> UpdateHipervinculos()
        {
            throw new NotImplementedException();
        }
    }
}
