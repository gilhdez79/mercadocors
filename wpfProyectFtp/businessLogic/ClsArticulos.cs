﻿
using BibliotecaClasess.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using wpfProyectFtp.Commons;

namespace wpfProyectFtp.businessLogic
{
    public class ClsArticulos
    {
        public OperationResult<List<Articulo>> GetListaArticulos()
        {
            List<Articulo> listaArticulos = new List<Articulo>();
            OperationResult<List<Articulo>> _objArticulo = new OperationResult<List<Articulo>>();
            try
            {
                Articulo objUsuario = new Articulo();
                APIConnect<Articulo> apiUsuarios = new APIConnect<Articulo>();
                string recurso = $"articulos/GetArticulos";

                var result = apiUsuarios.GetAsync(objUsuario, recurso).Result;
                 _objArticulo = JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<List<Articulo>>>(result);

               
            }
            catch (Exception ex)
            {
                _objArticulo.InfoMensaje.Message = ex.Message.ToString();
                _objArticulo.InfoMensaje.TipoMensaje = TipoMensaje.Error;
                throw ex;
            }

            return _objArticulo;
        }
    }
}
