using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Data.Entity;
using wpfProyectFtp.database;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using wpfProyectFtp.businessLogic;
using System.Collections.ObjectModel;
using BibliotecaClasess.Models;
using wpfProyectFtp.Commons;
using wpfProyectFtp.Modelos;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Threading;

namespace wpfProyectFtp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ProgressBar progressBar = new ProgressBar();
        public ObservableCollection<Articulo> ArticulosList = new ObservableCollection<Articulo>();
        FunctionsCommons functiosCommons = new FunctionsCommons();
        ModelEstructuraRuta objModelRuta = new ModelEstructuraRuta();
        ObsrvButtons obsrvButtons = ObsrvButtons.GetButtonsInfo();
        public string urlEstructura = string.Empty;
        private static BackgroundWorker worker;
        public bool _btnBuscarEnabled = false;  

        public MainWindow()
        {
            InitializeComponent();

            GetArticulos();
            GetFraccion();
            GetInciso();
            GetAnios();
            GetClaseArchivo();
            GetTrimestres();

            //btnCopiar.IsEnabled = false;
            //btnGeneraLink.IsEnabled = false;
            // btnSubirPortal.IsEnabled = false;
            //  obsrvButtons.BtnBuscarEnable = false;



            btnBuscar.DataContext = obsrvButtons;
            btnGeneraLink.DataContext = obsrvButtons;
            btnSubirPortal.DataContext = obsrvButtons;
            btnCopiar.DataContext = obsrvButtons;

            gvProgress.Visibility = Visibility.Hidden;
        
        }

        public void Upload()

        {
            string rutaArhivo = @"C:\AyuntamientoCardenas\Documentos Transparencia\Articulo 78\fracc_XIV\Actas Secretaria_2021\2do_trimestre\2DO TRIM 2021 Acta 77 SESION 30 ABRIL.pdf";
            string ftpServerIP = "transparencia.cardenas.gob.mx";
            FileInfo fileInf = new FileInfo(rutaArhivo);

            //string uri = "ftp://" + ftpServerIP + "/" + fileInf.Name;

            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + "/httpdocs/Transparencia_2019/art_78/fracc_VII/4to_trimestre/" + fileInf.Name));

            // Provide the WebPermission Credintials

            reqFTP.Credentials = new NetworkCredential("transpa1", "yt%vrX7!%7$");

            // By default KeepAlive is true, where the control connection is not closed

            // after a command is executed.

            reqFTP.KeepAlive = false;

            // Specify the command to be executed.

            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            // Specify the data transfer type.

            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file

            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb

            int buffLength = 2048;

            byte[] buff = new byte[buffLength];

            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file to be uploaded

            FileStream fs = fileInf.OpenRead();

            try

            {

                // Stream to which the file to be upload is written

                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time

                contentLen = fs.Read(buff, 0, buffLength);

                // Until Stream content ends

                while (contentLen != 0)

                {

                    // Write Content from the file stream to the FTP Upload Stream

                    strm.Write(buff, 0, contentLen);

                    contentLen = fs.Read(buff, 0, buffLength);

                }

                // Close the file stream and the Request Stream

                strm.Close();

                fs.Close();

            }

            catch (Exception ex)

            {

                Console.WriteLine(ex.Message);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SubirArchivo();
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ModelEstructuraRuta _objModelRuta = e.Argument as ModelEstructuraRuta;

            if (worker.CancellationPending)
            {
                e.Cancel = true;
                
            }
            worker.ReportProgress(1);
            Thread.Sleep(200);
            subirArchivoWorker(_objModelRuta);
           // worker.ReportProgress(100);
        
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
               // StatusTextBox.Text = "Cancelled";
            }
            else
            {
                //  StatusTextBox.Text = "Completed";
                gvProgress.Visibility = Visibility.Hidden;

                MessageBox.Show("Se ha Guardado el Archivo y se generó su hypervinculo..", "Información", MessageBoxButton.OK, MessageBoxImage.Information);

            }

        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double percent = (e.ProgressPercentage * 100) / 50;

            //ProgressBar1.Value = Math.Round(percent, 0);

            //ListBox1.Items.Add(new ListBoxItem { Content = e.ProgressPercentage + " item added" });
            //StatusTextBox.Text = Math.Round(percent, 0) + "% percent completed";


        }



        public void SubirArchivo(){
            //  Upload();

            //EstructuraArchivos estructuraArchivos = new EstructuraArchivos();

            //estructuraArchivos.InsertarDatosArchivos();
            objModelRuta.UrlFtp = Recursos.UriFtp_Text;
            string hypervinculo = objModelRuta.GeneraHyperVinculo();
            objModelRuta.EstructuraEnCarpetaFTP = hypervinculo;
            // objModelRuta.HyperVinculo = objModelRuta.GeneraHyperVinculo();
            txtbootomBar.Content = string.Concat(objModelRuta.UrlFtp, hypervinculo);
            txtHyperlink.Text = string.Concat(objModelRuta.UrlFtp, hypervinculo, objModelRuta._ModelArchivo.NombreArchivo);
            objModelRuta.HyperVinculo = string.Concat(objModelRuta.UrlFtp, hypervinculo, "/", objModelRuta._ModelArchivo.NombreArchivo);

            // btnSubirPortal.IsEnabled = true;
            this.obsrvButtons.BtnSubirPortalEnable = true;

            txtHyperlink.Text = objModelRuta.HyperVinculo;
        }

        public OperationResult<Datoslink> subirArchivoWorker(ModelEstructuraRuta _objModelRuta)
        {
            Datoslink datoslink = new Datoslink()
            {

                Link = _objModelRuta.HyperVinculo,
                NombreArchivo = _objModelRuta._ModelArchivo.NombreArchivo,
                PesoKb = _objModelRuta._ModelArchivo.PesoMB,
                UrlCarpeta = _objModelRuta._ModelArchivo.RutaArchivo,
                LinklHtml = _objModelRuta.HyperVinculo,
                FechaCreacion = DateTime.Now.ToString(),

            };

            var result = FnSubeArchivo(datoslink);

            return result;
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
          

            ModelArchivo modelArchivo = new ModelArchivo();
             OpenFileDialog openFileDialog = new OpenFileDialog();
             openFileDialog.Filter = "Pdf Files|*.pdf";

            if (openFileDialog.ShowDialog() == true)
             {
                txtbootomBar.Content = openFileDialog.FileName; //File.ReadAllText(openFileDialog.FileName);

                 FileInfo fi = new FileInfo(openFileDialog.FileName);
                txtbootomBar.Content= fi.Name;

                modelArchivo.NombreArchivo = fi.Name;
                modelArchivo.PesoMB = fi.Length.ToString();
                modelArchivo.RutaArchivo = openFileDialog.FileName;
                modelArchivo.FechaCreacion = fi.CreationTime.ToString();

                objModelRuta._ModelArchivo = modelArchivo;

              //  btnGeneraLink.IsEnabled = true;

                txtUrlGenerada.Text= openFileDialog.FileName;

                this.obsrvButtons.BtnGeneraLinkEnable = true;


            }
            
          

            /*   ClsArticulos clsArticulos = new ClsArticulos();

               var listaArticulos = clsArticulos.GetListaArticulos();

               cbxArticulos.ItemsSource = listaArticulos; */
            //ClsAdministraFTP objFtp = new ClsAdministraFTP();
            //objFtp.ExistFoldersintoDirectory("","");

        }

        private void GetArticulos() {

            ClsArticulos clsArticulos = new ClsArticulos();
            List<Articulo> listaArticulos = new List<Articulo>();

            var _objReturn = clsArticulos.GetListaArticulos();

            if (_objReturn.Success)
            {
                // MessageBox.Show("Se encontraron los Articulos","Informacion" ,MessageBoxButton.OK);

                if (_objReturn.ObjReturn.Count > 0)
                {
                    listaArticulos = _objReturn.ObjReturn;

                    cbxArticulos.ItemsSource = listaArticulos;
                    cbxArticulos.DisplayMemberPath = "EstructuraCarpeta";
                    cbxArticulos.SelectedValuePath = "Id";
                }
            }
            else
            {
                MessageBox.Show("No se encontraron datos", "Informacion");
            }
        }
        private void GetFraccion() {
            ClsFracciones clsFracciones = new ClsFracciones();
            List<Fraccion> listaItems = new List<Fraccion>();

            var _objReturn = clsFracciones.GetListaFracciones();

            if (_objReturn.Success)
            {
                // MessageBox.Show("Se encontraron los Articulos","Informacion" ,MessageBoxButton.OK);

                if (_objReturn.ObjReturn.Count > 0)
                {
                    listaItems = _objReturn.ObjReturn;

                    cbxFraccion.ItemsSource = listaItems;
                    cbxFraccion.DisplayMemberPath = "EstructuraCarpeta";
                    cbxFraccion.SelectedValuePath = "Id";
                }
            }
            else
            {
                MessageBox.Show("No se encontraron datos", "Informacion");
            }
        }
        private void GetInciso()
        {
            ClsInciso clsInciso = new ClsInciso();
            List<Elementosclase> listaItems = new List<Elementosclase>();

            var _objReturn = clsInciso.GetListaElementosClases();

            if (_objReturn.Success)
            {
                // MessageBox.Show("Se encontraron los Articulos","Informacion" ,MessageBoxButton.OK);

                if (_objReturn.ObjReturn.Count > 0)
                {
                    listaItems = _objReturn.ObjReturn;

                    cbxInciso.ItemsSource = listaItems;
                    cbxInciso.DisplayMemberPath = "Clase";
                    cbxInciso.SelectedValuePath = "Id";
                }
            }
            else
            {
                MessageBox.Show("No se encontraron datos", "Informacion");
            }
        }
        private void GetAnios()
        {

            var annios = functiosCommons.GetListaAnnios();

            cbxAnnios.ItemsSource = annios;
            //cbxInciso.DisplayMemberPath = "Clase";
            //cbxInciso.SelectedValuePath = "Id";
            cbxAnnios.SelectedIndex = 0;
        }

        private void GetClaseArchivo()
        {

            ClsClaseArchivo clsClaseArchivo = new ClsClaseArchivo();
            List<Elemento> listaItems = new List<Elemento>();

            var _objReturn = clsClaseArchivo.GetListaClaseArchivo();

            if (_objReturn.Success)
            {
                // MessageBox.Show("Se encontraron los Articulos","Informacion" ,MessageBoxButton.OK);

                if (_objReturn.ObjReturn.Count > 0)
                {
                    listaItems = _objReturn.ObjReturn;

                    cbxClasificacionFiles.ItemsSource = listaItems;
                    cbxClasificacionFiles.DisplayMemberPath = "NombreElemento";
                    cbxClasificacionFiles.SelectedValuePath = "Id";
                }
            }
            else
            {
                MessageBox.Show("No se encontraron datos", "Informacion");
            }
        }

        private void GetTrimestres()
        {
            Dictionary<string, string> lista_Trim = new Dictionary<string, string>();

            lista_Trim = functiosCommons.GetListTrimestres();
            cbxTrimestre.ItemsSource = lista_Trim;
            cbxTrimestre.DisplayMemberPath = "Value";
            cbxTrimestre.SelectedValuePath = "Key";

        }

        private void cbxClasificacionFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cbxAnnios.SelectedItem!= null)
            {
                var typeItem = (Elemento)cbxClasificacionFiles.SelectedItem;
                string value = typeItem.Id.ToString();

                objModelRuta.ClasificacionArchivo = typeItem.NombreElemento.ToString();
                //  objModelRuta.Annio = typeItem.NombreElemento.ToString();

                switch (value)
                {
                    case "1":
                        {
                            cbxArticulos.IsEnabled = true;
                            cbxFraccion.IsEnabled = true;
                            cbxInciso.IsEnabled = true;
                            cbxTrimestre.IsEnabled = true;
                            this.obsrvButtons.DisabledButtons();

                            break;
                        }
                    default:
                        {
                            cbxArticulos.IsEnabled = false;
                            cbxFraccion.IsEnabled = false;
                            cbxInciso.IsEnabled = false;
                            cbxTrimestre.IsEnabled = false;

                            cbxArticulos.SelectedIndex = -1;
                            cbxFraccion.SelectedIndex = -1;
                            cbxInciso.SelectedIndex = -1;
                            cbxTrimestre.SelectedIndex = -1;

                            this.obsrvButtons.DisabledButtons();
                            this.obsrvButtons.BtnBuscarEnable = true;
                        
                             


                            break;
                        }

                }

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;
            }
        }

        private void cbxAnnios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxAnnios.SelectedItem != null)
            {
                var typeItem = cbxAnnios.SelectedItem;
                string value = typeItem.ToString();

                objModelRuta.Annio = value;

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;
            }
        }

        private void cbxArticulos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var typeItem = (Articulo)cbxArticulos.SelectedItem;
            if (cbxArticulos.SelectedItem !=null)
            {
                string value = typeItem.Id.ToString();
                objModelRuta.Articulo = typeItem.NombreArticulo.ToString();

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;
            }


        }

        private void cbxFraccion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxFraccion.SelectedItem != null)
            {
                var typeItem = (Fraccion)cbxFraccion.SelectedItem;
                string value = typeItem.Id.ToString();
                objModelRuta.Fraccion = typeItem.Nombre.ToString();

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;
            }
        }

        private void cbxInciso_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxInciso.SelectedItem !=null)
            {
                var typeItem = (Elementosclase)cbxInciso.SelectedItem;

                string value = typeItem.Id.ToString();
                objModelRuta.Inciso = typeItem.Clase.ToString();

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;
            }
        }

        private void cbxTrimestre_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxTrimestre.SelectedItem != null)
            {
                var typeItem = cbxTrimestre.SelectedItem;
                string value = typeItem.ToString();
                var json = JsonConvert.SerializeObject(typeItem);
                var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                objModelRuta.Trimestre = dictionary["Key"];

                txtHyperlink.Text = string.Empty;
                txtUrlGenerada.Text = string.Empty;

                this.obsrvButtons.BtnBuscarEnable = true;
            }
        }

        private void btnCopiar_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(txtHyperlink.Text);
        }

        private void btnSubirPortal_Click(object sender, RoutedEventArgs e)
        {
            ClsDatosLink objDatosLink = new ClsDatosLink();

            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;

            int maxItems = 50;
            //ProgressBar1.Minimum = 1;
            //ProgressBar1.Maximum = 100;

            //StatusTextBox.Text = "Starting...";
          
            //Subir rchivo
            try
            {
                gvProgress.Visibility = Visibility.Visible;
                ClsAdministraFTP clsAdministraFTP = new ClsAdministraFTP();
                bool subioArchivo = true;  //clsAdministraFTP.SubirArchivoaFTP(objModelRuta);
                           
                if (subioArchivo)
                {

            
                   // btnCopiar.IsEnabled = true;
                    this.obsrvButtons.BtnCopiarEnable = true;

                    //Guardar los datos en la base de DAtos.
                 
                    worker.RunWorkerAsync(objModelRuta);

                   

                }
                else
                {
                    //btnCopiar.IsEnabled = false;
                    this.obsrvButtons.BtnCopiarEnable = false;
                }
               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }

        public OperationResult<Datoslink> FnSubeArchivo(Datoslink datoslink)
        {
            ClsDatosLink objDatosLink = new ClsDatosLink();
            var resultado = objDatosLink.InsertHipervinculos(datoslink);


            return resultado;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            cbxArticulos.IsEnabled = false;
            cbxFraccion.IsEnabled = false;
            cbxInciso.IsEnabled = false;
            cbxTrimestre.IsEnabled = false;

            cbxArticulos.SelectedIndex = -1;
            cbxFraccion.SelectedIndex = -1;
            cbxInciso.SelectedIndex = -1;
            cbxTrimestre.SelectedIndex = -1;

            txtbootomBar.Content = "";
            txtHyperlink.Text = string.Empty;
            txtUrlGenerada.Text = string.Empty;

             
            this.obsrvButtons.DisabledButtons();
            this.obsrvButtons.BtnBuscarEnable = true;
        }
    }
}
