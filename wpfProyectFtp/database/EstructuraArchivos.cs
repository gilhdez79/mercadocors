﻿using BibliotecaClasess.models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.interfaces;
using wpfProyectFtp.Modelos;

namespace wpfProyectFtp.database
{
 public   class EstructuraArchivos
    {
        private string _Url;

        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        private string _LinkHtml;

        public string LinkHtml
        {
            get { return _LinkHtml; }
            set { _LinkHtml = value; }
        }

        private string _EstructuraCarpetaFtp;

        public string EstructuraCarpetaFtp
        {
            get { return _EstructuraCarpetaFtp; }
            set { _EstructuraCarpetaFtp = value; }
        }

        public EstructuraArchivo InsertarDatosArchivos()
        {
            EstructuraArchivo estructuraArchivo = new EstructuraArchivo();
            string connectionString = "Server=localhost;Port=3306;Database=dbArchivosTransparencia;Uid=root;password=123456;";
            //  MySqlConnection conexion = new MySqlConnection(connectionString);
            MySqlConnection conexion = new MySqlConnection(connectionString);
            
                //Codigo 
                MySqlCommand cmd = new MySqlCommand();

                cmd.Connection = conexion;
                cmd.CommandText = $"select * from datosLinks";
                conexion.Open();
            using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        estructuraArchivo.Aticulo =  reader["id"].ToString();
                        estructuraArchivo.LinkGenerado = reader["Link"].ToString();
                    }
                }

            return estructuraArchivo;
        }
    }
}
