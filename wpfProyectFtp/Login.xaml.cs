﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using wpfProyectFtp.Commons;
using BibliotecaClasess.Models;
using Newtonsoft.Json;

namespace wpfProyectFtp
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Usuario objUsuario = new Usuario();

            objUsuario.Usuario1 = txtUsuario.Text;
            objUsuario.Pasword = txtPassword.Password;

            APIConnect<Usuario> apiUsuarios = new APIConnect<Usuario>();
            string recurso = $"usuarios/GetDatosUser/{2}";

          var result =  apiUsuarios.GetAsync(objUsuario, recurso).Result;
          OperationResult<Usuario> _objUsuario=  JsonConvert.DeserializeObject<BibliotecaClasess.Models.OperationResult<Usuario>>(result);

            if (_objUsuario.Success)
            {
               // MessageBox.Show("Contraseña Correcta", "Acceso al Sistema");
                this.Hide();
                MainWindow main = new MainWindow();
                main.ShowDialog();
               

            }
            else
            {
                MessageBox.Show("Contraseña Incorrecta", "Acceso al Sistema");
            }
           
        }
    }
}
