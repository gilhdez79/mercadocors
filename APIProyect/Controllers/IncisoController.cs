﻿using APIProyect.businessLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IncisoController : Controller
    {
        [HttpGet]
        [Route("GetInciso")]
        public IActionResult GetInciso()
        {

            BsInciso bsInciso = new BsInciso();

            var listaInciso = bsInciso.GetListaInciso();

            return Ok(listaInciso);
        }
    }
}
