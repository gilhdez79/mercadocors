﻿using APIProyect.businessLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ClaseArchivoController : ControllerBase
    {
      
       
            [HttpGet]
            [Route("GetClaseArchivos")]
            public IActionResult GetClaseArchivos()
            {

                BsClaseArchivo bsClaseArchiv = new BsClaseArchivo();

                var listaArticulos = bsClaseArchiv.GetListaElementos();

                return Ok(listaArticulos);
            }
        
    }
}
