﻿using APIProyect.businessLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticulosController : ControllerBase
    {
        [HttpGet]
        [Route("GetArticulos")]
        public IActionResult GetArticulos()
        {
             
            BsArticulo bsArticulo = new BsArticulo();

            var listaArticulos = bsArticulo.GetListaArticulos();

            return Ok(listaArticulos);
        }
    }
}
