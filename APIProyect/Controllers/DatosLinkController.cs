﻿using APIProyect.businessLogic;
using APIProyect.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DatosLinkController : ControllerBase
    {
        [HttpPost]
        public IActionResult Post([FromBody]Datoslink datoslink)
        {

            BsDatosLink bsDatosLink = new BsDatosLink();

            var inserDatos = bsDatosLink.InsertarDatosHipervinculo(datoslink);

            return Ok(inserDatos);
        }
    }
}
