﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIProyect.businessLogic;
using APIProyect.Models;

namespace APIProyect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        [HttpGet]
        [Route("AuthenticaUser")]
        [Produces("application/json")]
        public IActionResult AuthenticaUser()
        {
            BsUsuario bsUsuario = new BsUsuario();


            var listaUsuarios = bsUsuario.GetDatosAutenticacion("Usuario1", "PassWord");

            return Ok(listaUsuarios);
        }

        [HttpGet]
        [Route("GetDatosUser/{id}")]
        public IActionResult GetDatosUser(int id)
        {
            if (id == null)
            {
                return NoContent();
            }
            BsUsuario bsUsuario = new BsUsuario();

            var listaUsuarios = bsUsuario.GetDatosUsuarios(id);

            return Ok(listaUsuarios);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Usuario user)
        {
            if (string.IsNullOrWhiteSpace(user.Nombre))
            {
                return NoContent();
            }
            BsUsuario bsUsuario = new BsUsuario();

            var listaUsuarios = bsUsuario.RegistraUser(user);

            return Ok(listaUsuarios);
        }
    }
}
