﻿using APIProyect.businessLogic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FraccionController : ControllerBase
    {

        [HttpGet]
        [Route("GetFracciones")]
        public IActionResult GetFracciones()
        {

            BsFracciones bsFraccion = new BsFracciones();

            var listaFracciones = bsFraccion.GetListaFracciones();

            return Ok(listaFracciones);
        }
    }
}
