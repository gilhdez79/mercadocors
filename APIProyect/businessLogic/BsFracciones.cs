﻿using APIProyect.interfaces;
using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.businessLogic
{
    public class BsFracciones : IFracciones
    {
        public OperationResult<Fraccion> DeleteFracciones(int idFraccion)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<Fraccion>> GetListaFracciones()
        {
            OperationResult<IEnumerable<Fraccion>> operationResult = new OperationResult<IEnumerable<Fraccion>>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    IEnumerable<Fraccion> usr = (IEnumerable<Fraccion>)db.Fraccions.ToList();

                    if (usr != null)
                    {
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Se obtubieron los datos Correctamente", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Obtener los datos", TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;
        }

        public OperationResult<Fraccion> InsertFracciones(Fraccion itemFracciones)
        {
            throw new NotImplementedException();
        }

        public OperationResult<Fraccion> UpdateFracciones(Fraccion itemFracciones)
        {
            throw new NotImplementedException();
        }
    }
}
