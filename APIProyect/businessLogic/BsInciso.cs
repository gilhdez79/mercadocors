﻿using APIProyect.interfaces;
using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.businessLogic
{
    public class BsInciso : IInciso
    {
        public OperationResult<Elementosclase> DeleteInciso(int idFraccion)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<Elementosclase>> GetListaInciso()
        {
            OperationResult<IEnumerable<Elementosclase>> operationResult = new OperationResult<IEnumerable<Elementosclase>>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    IEnumerable<Elementosclase> usr = (IEnumerable<Elementosclase>)db.Elementosclases.ToList();

                    if (usr != null)
                    {
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Se obtubieron los datos Correctamente", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Obtener los datos", TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;
        }

        public OperationResult<Elementosclase> InsertInciso(Elementosclase itemFracciones)
        {
            throw new NotImplementedException();
        }

        public OperationResult<Elementosclase> UpdateInciso(Elementosclase itemFracciones)
        {
            throw new NotImplementedException();
        }
    }
}
