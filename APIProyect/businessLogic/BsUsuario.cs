﻿using APIProyect.interfaces;
using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIProyect.Commons;

namespace APIProyect.businessLogic
{
    public class BsUsuario : IUsuario
    {
        public OperationResult<Usuario> GetDatosAutenticacion(string usuario, string contrasenia)
        {
            var operationResult = new OperationResult<Usuario>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    var usr = db.Usuarios.FirstOrDefault(u=> u.Usuario1 == usuario && u.Pasword == contrasenia);

                    if (usr != null)
                    {
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "El usuario Tiene Permiso", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Obtener los datos", TipoMensaje = TipoMensaje.Error };
                 
            }

            return operationResult;
        }

        public OperationResult<Usuario> RegistraUser(Usuario objUser)
        {
            ClsEncript encriptar = new ClsEncript();
            var operationResult = new OperationResult<Usuario>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                   var usr = db.Usuarios.FirstOrDefault(u => u.CorreoElectronico ==objUser.CorreoElectronico);

                    if (usr !=null)
                    {
                        operationResult.Success = false;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Ya existe un correo registrado", TipoMensaje = TipoMensaje.Error };
                    }
                    else
                    {
                        objUser.Pasword = encriptar.Encrypt2(objUser.Pasword);
                        db.Add(objUser);
                        db.SaveChanges();

                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Usuario Registrado Correctamente", TipoMensaje = TipoMensaje.Informacion };
                    }
                   
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = ex.Message, TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;

        }

        public OperationResult<Usuario> GetDatosUsuarios(int idUsuario)
        {
            ClsEncript encriptar = new ClsEncript();
            var operationResult = new OperationResult<Usuario>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    var usr = db.Usuarios.FirstOrDefault(u => u.Id == idUsuario);

                    if (usr == null)
                    {
                        operationResult.Success = false;
                        operationResult.InfoMensaje = new SystemMessage { Message = "No se encontraron datos de usuario", TipoMensaje = TipoMensaje.Error };
                    }
                    else
                    {
                        usr.Pasword = encriptar.Decrypt(usr.Pasword);

                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Operacion Exitosa", TipoMensaje = TipoMensaje.Informacion };
                        operationResult.SetSuccesObject(usr);
                    }

                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = ex.Message, TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;

        }

    }
}
