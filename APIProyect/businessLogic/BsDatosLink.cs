﻿using System;
using APIProyect.interfaces;
using APIProyect.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wpfProyectFtp.interfaces;
using wpfProyectFtp.models;
using Microsoft.Extensions.Configuration;

namespace APIProyect.businessLogic
{
    public class BsDatosLink : IDatosLinks
    {

        public bool EliminarDatosHipervinculo(EstructuraArchivo estructuraArchivo)
        {
            throw new NotImplementedException();
        }

        public List<EstructuraArchivo> GetEstructuraArchivos()
        {
            throw new NotImplementedException();
        }

        public OperationResult<Datoslink> InsertarDatosHipervinculo(Datoslink datoslink)
        {
           
            OperationResult<Datoslink> operationResult = new OperationResult<Datoslink>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    db.Datoslinks.Add(datoslink);

                    db.SaveChanges();

                     
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Se Guardaron  los datos Correctamente", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(datoslink);
                     
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Guardar los datos", TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;
        }
    }
}
