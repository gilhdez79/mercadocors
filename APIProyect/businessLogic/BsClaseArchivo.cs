﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIProyect.interfaces;
using APIProyect.Models;

namespace APIProyect.businessLogic
{
    public class BsClaseArchivo : IClaseArchivo
    {
        public OperationResult<Elemento> DeleteElementos(int idElementos)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<Elemento>> GetListaElementos()
        {
            OperationResult<IEnumerable<Elemento>> operationResult = new OperationResult<IEnumerable<Elemento>>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    IEnumerable<Elemento> usr = (IEnumerable<Elemento>)db.Elementos.ToList();

                    if (usr != null)
                    {
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Se obtubieron los datos Correctamente", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Obtener los datos", TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;
        }

        public OperationResult<Elemento> InsertElementos(Elemento itemElementos)
        {
            throw new NotImplementedException();
        }

        public OperationResult<Elemento> UpdateElementos(Elemento itemElementos)
        {
            throw new NotImplementedException();
        }
    }
}
