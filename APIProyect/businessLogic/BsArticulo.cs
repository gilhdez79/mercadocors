﻿using APIProyect.interfaces;
using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.businessLogic
{
    public class BsArticulo : IArticulo
    {
        public OperationResult<Articulo> DeleteArticulo(int idArticulo)
        {
            throw new NotImplementedException();
        }

        public OperationResult<Articulo> InsertArticulo(Articulo itemArticulo)
        {
            throw new NotImplementedException();
        }

        public OperationResult<Articulo> UpdateArticulo(Articulo itemArticulo)
        {
            throw new NotImplementedException();
        }


      public  OperationResult<IEnumerable<Articulo>> GetListaArticulos()
        {
            OperationResult<IEnumerable<Articulo>> operationResult = new OperationResult<IEnumerable<Articulo>>();

            try
            {
                using (var db = new dbarchivostransparenciaContext())
                {
                    IEnumerable<Articulo> usr = (IEnumerable<Articulo>)db.Articulos.ToList();

                    if (usr != null)
                    {
                        operationResult.Success = true;
                        operationResult.InfoMensaje = new SystemMessage { Message = "Se obtubieron los datos Correctamente", TipoMensaje = TipoMensaje.Default };
                        operationResult.SetSuccesObject(usr);
                    }
                }
            }
            catch (Exception ex)
            {
                operationResult.Success = false;
                operationResult.InfoMensaje = new SystemMessage { Message = "Error al Obtener los datos", TipoMensaje = TipoMensaje.Error };

            }

            return operationResult;
        }
    }
}
