﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIProyect.Models
{
    public partial class Mercadoparticipante
    {
        public int Idparticipante { get; set; }
        public int Idmercado { get; set; }
        public bool? Activo { get; set; }
    }
}
