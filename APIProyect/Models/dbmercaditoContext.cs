﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace APIProyect.Models
{
    public partial class dbmercaditoContext : DbContext
    {
        public dbmercaditoContext()
        {
        }

        public dbmercaditoContext(DbContextOptions<dbmercaditoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Itemmercado> Itemmercados { get; set; }
        public virtual DbSet<Mercadoparticipante> Mercadoparticipantes { get; set; }
        public virtual DbSet<Negociopropietario> Negociopropietarios { get; set; }
        public virtual DbSet<Tblpartipante> Tblpartipantes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("server=localhost;port=3306;user=rootprueba;password=123456;database=dbmercadito");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Itemmercado>(entity =>
            {
                entity.ToTable("itemmercado");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(200)
                    .HasColumnName("direccion")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Horario)
                    .HasMaxLength(100)
                    .HasColumnName("horario")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NombreResponsable)
                    .HasMaxLength(100)
                    .HasColumnName("nombreResponsable")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.TelefonoContaco)
                    .HasMaxLength(12)
                    .HasColumnName("telefono_contaco")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UbicacioLocalidad)
                    .HasMaxLength(100)
                    .HasColumnName("ubicacio_localidad")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Mercadoparticipante>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("mercadoparticipantes");

                entity.Property(e => e.Activo)
                    .HasColumnType("bit(1)")
                    .HasColumnName("activo")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Idmercado)
                    .HasColumnType("int(11)")
                    .HasColumnName("idmercado");

                entity.Property(e => e.Idparticipante)
                    .HasColumnType("int(11)")
                    .HasColumnName("idparticipante");
            });

            modelBuilder.Entity<Negociopropietario>(entity =>
            {
                entity.ToTable("negociopropietario");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("id");

                entity.Property(e => e.Antiguedad)
                    .HasMaxLength(10)
                    .HasColumnName("antiguedad")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Domicilio)
                    .HasMaxLength(400)
                    .HasColumnName("domicilio")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Facebook)
                    .HasMaxLength(100)
                    .HasColumnName("facebook")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Giro)
                    .HasMaxLength(50)
                    .HasColumnName("giro")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IdPropietario)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("id_propietario")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NoEmpleados)
                    .HasColumnType("int(11)")
                    .HasColumnName("no_empleados")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Producto)
                    .HasMaxLength(100)
                    .HasColumnName("producto")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Whatsapp)
                    .HasMaxLength(20)
                    .HasColumnName("whatsapp")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Tblpartipante>(entity =>
            {
                entity.ToTable("tblpartipantes");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("id");

                entity.Property(e => e.Amaterno)
                    .HasMaxLength(100)
                    .HasColumnName("amaterno")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Apaterno)
                    .HasMaxLength(100)
                    .HasColumnName("apaterno")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Cedula)
                    .HasMaxLength(20)
                    .HasColumnName("cedula")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Edad)
                    .HasColumnType("int(11)")
                    .HasColumnName("edad")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("nombre");

                entity.Property(e => e.Rfc)
                    .HasMaxLength(20)
                    .HasColumnName("rfc")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Sexo)
                    .HasMaxLength(1)
                    .HasColumnName("sexo")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
