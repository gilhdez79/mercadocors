﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIProyect.Models
{
    public partial class Tblpartipante
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Apaterno { get; set; }
        public string Amaterno { get; set; }
        public int? Edad { get; set; }
        public string Sexo { get; set; }
        public string Cedula { get; set; }
        public string Rfc { get; set; }
    }
}
