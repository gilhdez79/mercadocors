﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIProyect.Models
{
    public partial class Itemmercado
    {
        public int Id { get; set; }
        public string UbicacioLocalidad { get; set; }
        public string Direccion { get; set; }
        public DateTime? Fecha { get; set; }
        public string Horario { get; set; }
        public string TelefonoContaco { get; set; }
        public string NombreResponsable { get; set; }
    }
}
