﻿using System;
using System.Collections.Generic;

#nullable disable

namespace APIProyect.Models
{
    public partial class Negociopropietario
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Giro { get; set; }
        public string Producto { get; set; }
        public string Domicilio { get; set; }
        public string Antiguedad { get; set; }
        public int? NoEmpleados { get; set; }
        public string Whatsapp { get; set; }
        public string Facebook { get; set; }
        public long? IdPropietario { get; set; }
    }
}
