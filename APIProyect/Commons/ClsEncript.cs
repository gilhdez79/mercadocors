﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using APIProyect.interfaces;

namespace APIProyect.Commons
{
    public class ClsEncript : IEncript
    {
        public string Desencriptar(string cadena, string valor)
        {
            string strEnript = string.Empty;

            byte[] cadenaBytes = Convert.FromBase64String(cadena);
            byte[] claveBytes = Encoding.UTF8.GetBytes(valor);

            RijndaelManaged rij = new RijndaelManaged();

            rij.Mode = CipherMode.ECB;
            rij.BlockSize = 256;

            rij.Padding = PaddingMode.Zeros;

            ICryptoTransform deseencriptador;
            deseencriptador = rij.CreateDecryptor(claveBytes, rij.IV);

            MemoryStream memStream = new MemoryStream(cadenaBytes);

            CryptoStream cifradoStream;

            cifradoStream = new CryptoStream(memStream, deseencriptador, CryptoStreamMode.Read);

            StreamReader lectorStream = new StreamReader(cifradoStream);

            string resultado = lectorStream.ReadToEnd();

            memStream.Close();
            cifradoStream.Close();

            return resultado;
        }

        public string Encrptar(string cadena, string clave)
        {
            string strEnript = string.Empty;

            byte[] cadenaBytes = Encoding.UTF8.GetBytes(cadena);
            byte[] claveBytes = Encoding.UTF8.GetBytes(clave);

            RijndaelManaged rij = new RijndaelManaged();

            rij.Mode = CipherMode.ECB;
            rij.BlockSize = 256;

            rij.Padding = PaddingMode.Zeros;

            ICryptoTransform encriptador;
            encriptador = rij.CreateEncryptor(claveBytes,rij.IV);

            MemoryStream memStream = new MemoryStream();

            CryptoStream cifradoStream;

            cifradoStream = new CryptoStream(memStream,encriptador, CryptoStreamMode.Write);
            cifradoStream.Write(cadenaBytes,0, cadenaBytes.Length);
            cifradoStream.FlushFinalBlock();
            byte[] cipherTextByte = memStream.ToArray();
            memStream.Close();
            cifradoStream.Close();

            return Convert.ToBase64String(cipherTextByte);
            
        }

        public string Encrypt2(string clearText)
        {
            string EncryptionKey = "USRCRD2021";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64,
0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "USRCRD2021";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64,
0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}
