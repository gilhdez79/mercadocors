﻿using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
   public  interface IClaseArchivo
    {
        public OperationResult<IEnumerable<Elemento>> GetListaElementos();
        public OperationResult<Elemento> InsertElementos(Elemento itemElementos);
        public OperationResult<Elemento> UpdateElementos(Elemento itemElementos);
        public OperationResult<Elemento> DeleteElementos(int idElementos);
    }
}
