﻿using APIProyect.Models;
using System;

using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.models;

namespace wpfProyectFtp.interfaces
{
    public interface IDatosLinks
    {
    

        public OperationResult<Datoslink> InsertarDatosHipervinculo(Datoslink estructuraArchivo);
        public bool EliminarDatosHipervinculo(EstructuraArchivo estructuraArchivo);
        
        public List<EstructuraArchivo> GetEstructuraArchivos();



    }
}
