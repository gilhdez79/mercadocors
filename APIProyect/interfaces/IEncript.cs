﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
    public interface IEncript
    {
        string Encrptar(string cadena, string valor);
        string Desencriptar(string cadena, string valor);
    }
}
