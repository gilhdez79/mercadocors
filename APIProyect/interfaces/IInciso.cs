﻿using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
  public  interface IInciso
    {
        public OperationResult<IEnumerable<Elementosclase>> GetListaInciso();
        public OperationResult<Elementosclase> InsertInciso(Elementosclase itemFracciones);
        public OperationResult<Elementosclase> UpdateInciso(Elementosclase itemFracciones);
        public OperationResult<Elementosclase> DeleteInciso(int idFraccion);
    }
}
