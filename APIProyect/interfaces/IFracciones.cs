﻿using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
    public interface IFracciones
    {
        public OperationResult<IEnumerable<Fraccion>> GetListaFracciones();
        public OperationResult<Fraccion> InsertFracciones(Fraccion itemFracciones);
        public OperationResult<Fraccion> UpdateFracciones(Fraccion itemFracciones);
        public OperationResult<Fraccion> DeleteFracciones(int idFraccion);
    }
}
