﻿using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
    public  interface IUsuario
    {
        public OperationResult<Usuario> RegistraUser(Usuario objUser);
        public OperationResult<Usuario> GetDatosAutenticacion(string usuario, string contrasenia);


    }
}
