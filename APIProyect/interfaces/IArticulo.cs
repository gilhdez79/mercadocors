﻿using APIProyect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIProyect.interfaces
{
    public interface IArticulo
    {
        public OperationResult<IEnumerable<Articulo>> GetListaArticulos();
        public OperationResult<Articulo> InsertArticulo(Articulo itemArticulo);
        public OperationResult<Articulo> UpdateArticulo(Articulo itemArticulo);
        public OperationResult<Articulo> DeleteArticulo(int idArticulo);

    }
}
