﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using wpfProyectFtp.interfaces;
using wpfProyectFtp.models;
using Microsoft.Extensions.Configuration;

namespace wpfProyectFtp.database
{

 public   class EstructuraArchivos
    {
        public readonly IConfiguration configuration;
        private string _Url;

        public EstructuraArchivos(IConfiguration config)
        {
            configuration = config;
        }
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        private string _LinkHtml;

        public string LinkHtml
        {
            get { return _LinkHtml; }
            set { _LinkHtml = value; }
        }

        private string _EstructuraCarpetaFtp;

        public string EstructuraCarpetaFtp
        {
            get { return _EstructuraCarpetaFtp; }
            set { _EstructuraCarpetaFtp = value; }
        }

        public EstructuraArchivo GetDatosLinks()
        {
            
            EstructuraArchivo estructuraArchivo = new EstructuraArchivo();
            //string connectionString = "Server=localhost;Port=3306;Database=dbArchivosTransparencia;Uid=root;password=123456;";
            var connectionString = configuration.GetConnectionString("ConnectionStrings:MysqlConn");

            //  MySqlConnection conexion = new MySqlConnection(connectionString);
            MySqlConnection conexion = new MySqlConnection(connectionString);
            
                //Codigo 
                MySqlCommand cmd = new MySqlCommand();

                cmd.Connection = conexion;
                cmd.CommandText = $"select * from datosLinks";
                conexion.Open();
            using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        estructuraArchivo.Aticulo =  reader["id"].ToString();
                        estructuraArchivo.LinkGenerado = reader["Link"].ToString();
                    }
                }

            

            

            return estructuraArchivo;
        }
    }
}
