﻿
using System;
using System.Collections.Generic;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Artirculofraccion
    {
        public Artirculofraccion()
        {
            Datoslinks = new HashSet<Datoslink>();
        }

        public short Id { get; set; }
        public short? IdArticulo { get; set; }
        public short? IdFraccion { get; set; }
        public short? IdClase { get; set; }
        public short? IdTrimestre { get; set; }

        public virtual Articulo IdArticuloNavigation { get; set; }
        public virtual Elementosclase IdClaseNavigation { get; set; }
        public virtual Fraccion IdFraccionNavigation { get; set; }
        public virtual Trimestre IdTrimestreNavigation { get; set; }
        public virtual ICollection<Datoslink> Datoslinks { get; set; }
    }
}
