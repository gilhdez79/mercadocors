﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Articulo
    {
        public Articulo()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string NombreArticulo { get; set; }
        public string EstructuraCarpeta { get; set; }
        public string Titulo { get; set; }


        [JsonIgnore]
        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
