﻿using BibliotecaClasess.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Usuariodependencium
    {
        public short? IdDependencia { get; set; }
        public short? IdUsuario { get; set; }

        public virtual Dependencium IdDependenciaNavigation { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
