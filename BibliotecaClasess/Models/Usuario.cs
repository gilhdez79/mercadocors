﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Usuario
    {
        public short Id { get; set; }
        public string Usuario1 { get; set; }
        public string Nombre { get; set; }
        public string ApellidoP { get; set; }
        public string Amaterno { get; set; }
        public string Pasword { get; set; }
        public string CorreoElectronico { get; set; }
    }
}
