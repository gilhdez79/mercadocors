﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace BibliotecaClasess.Models
{
    public class DbContextMysql
    {
        public string ConnectionString { get; set; }
        public DbContextMysql(string conectionString)
        {
            ConnectionString = conectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
    }
}
