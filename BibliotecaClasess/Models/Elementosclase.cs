﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Elementosclase
    {
        public Elementosclase()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string Clase { get; set; }

        [JsonIgnore]
        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
