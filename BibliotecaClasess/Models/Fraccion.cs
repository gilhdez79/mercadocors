﻿using BibliotecaClasess.Models;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace BibliotecaClasess.Models
{
    public partial class Fraccion
    {
        public Fraccion()
        {
            Artirculofraccions = new HashSet<Artirculofraccion>();
        }

        public short Id { get; set; }
        public string Nombre { get; set; }
        public string EstructuraCarpeta { get; set; }

        [JsonIgnore]
        public virtual ICollection<Artirculofraccion> Artirculofraccions { get; set; }
    }
}
